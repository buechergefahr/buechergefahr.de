serve:
	hugo server -D

# --minify: works on the final HTML output, not any (CSS, JS) assets
# see https://gohugo.io/hugo-pipes/bundling/ for the latter
# Note: as of mid 2021, '--minify' produces broken HTML. Disabled for now.
build: clean
	hugo

# requires: source .env
deploy: build
	rsync -axv --delete --ignore-errors --exclude .DS_Store --exclude .git public/ $(DEPLOY_SSH_USER)@$(DEPLOY_SSH_HOST):$(DEPLOY_SSH_PATH)

clean:
	rm -rf public

.PHONY:	serve clean
