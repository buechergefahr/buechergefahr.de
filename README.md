## Umgebung aufsetzen

```console
% git submodule add https://codeberg.org/buechergefahr/hugo-theme-buechergefahr.git themes/buechergefahr
```

`make`-Targets:

* `make serve`: build locally and start development server
* `make deploy`: build and deploy (via `rsync`)
* `make clean`: remove the `public` folder that might have been created

## Neue Folge anlegen

```sh
hugo new episode/<No>.md
```
