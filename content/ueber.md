---
title: Über
markup: html
permalink: /ueber/
---
<section id="features">
	<div class="container">

		<div class="row aln-center">
			<div class="col-4 col-6-medium col-12-small">
				<section>
					<a href="https://sr-rolando.com/" class="image featured icon solid"><img class="profilbild"
							src="/images/rolando.jpg" alt="Rolando" /></a>
					<header>
						<h3><a href="https://sr-rolando.com/">Señor Rolando</a></h3>
					</header>
					<p>Das Mädchen für alles.</p>
					<p>Spricht. Schreibt. Entwickelt. Verwaltet. Kocht auch Kaffee.</p>
				</section>
			</div>
			<div class="col-4 col-6-medium col-12-small">
				<section>
					<a href="https://lektomat.de/" class="image featured icon solid"><img src="/images/lektomat_logo.svg"
							alt="Lektomat der Büchergefahr" /></a>
					<header>
						<h3><a href="https://lektomat.de/">Lektomat</a></h3>
					</header>
					<p>Das Tool des Hauses.</p>
					<p>Zeigte ganz praktisch das, worüber wir hier sonst nur reden.</p>
				</section>
			</div>
			<div class="col-4 col-6-medium col-12-small">
				<section>
					<a href="#" class="image featured icon solid"><img src="/images/hoerbuecher_logo.svg"
							alt="Büchergefahr-Hörbücher" /></a>
					<header>
						<h3>Hörbücher</h3>
						<p>Indieautoren auf die Ohren.</p>
						<p>Hier sprachen wir gern Texte von Selfpublisherinnen ein, produzierten und veröffentlichten sie.</p>
				</section>
			</div>
		</div>

	</div>
</section>
<section id="main">
	<div class="container">

		<div id="content">
			<p>Und quer durch alles zog sich <a href="/podcast">der Podcast</a>. Wir redeten dort über das Marktgeschehen rund
				um Bücher, über Hörbücher und die Trends und Entwicklungen dieser und wir redeten selbstverständlich über den	Lektomat.</p>

			<h3>Credits</h3>

			<p>Die Musik im Podcast war in den Folgen 1 bis 28 <a
					href="https://soundcloud.com/spaceslab-go/forest-21-08-2014-2150">The Construct Forest Dwellers Of Orduhr
					Dance</a> und seit Folge 29 war es »T800« von <a href="https://about.me/jahzzar">Javier Suarez</a> (Jahzzar
				aka <a href="http://www.betterwithmusic.com">Betterwithmusic</a>, hier via <a
					href="https://vimeo.com/musicstore/track/80519/t800-by-jahzzar">Vimeo Music Store</a>, jeweils lizensiert als
				Creative Commons.
			<p>Das Coverbild im Podcast stammt von <a
					href="https://de.depositphotos.com/49566711/stock-photo-stacks-with-many-old-books.html">sorokopud @
					depositphotos</a>.</p>
		</div>

	</div>
</section>