---
title: Links
layout: page
permalink: /links/
---
# Links

## Ausprobieren von KI-Algorithemen

* [Talk to Transformer](https://talktotransformer.com/)

## Services zum Anwenden von u.a. KI bei der Textanalyse

* [Lektomat](https://lektomat.de/): Der hauseigene Dienst dieses Kanals.
* [QualiFiction](https://www.qualifiction.info/): Die wohl ersten auf dem deutschsprachigen Markt mit ihrer Software zur Analyse von Texten \& Bewertung von Bucherfolgen.
