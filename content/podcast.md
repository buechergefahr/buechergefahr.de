---
title: Podcast
layout: default_podcast
navigation: 1
permalink: /podcast/
---
<section id="main">
  <div class="container">
    <div id="content">
      {% comment %}
      Only include posts with audio (i.e. no 'product posts for books / downloadable audio books').
      {% endcomment %}
      {% if site.posts.first.audio %}
        {% assign post = site.posts.first %}
        {% include post.html %}
      {% endif %}
      {% assign current_post = site.posts.first %}
      {% for post in site.posts %}
        {% comment %}Only include posts with audio.{% endcomment %}
        {% if post.audio %}
          {% unless post.url == current_post.url %}
            {% include post_line.html %}
          {% endunless %}
        {% endif %}
      {% endfor %}
    </div>
  </div>
</section>
