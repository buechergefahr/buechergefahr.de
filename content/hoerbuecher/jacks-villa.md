---
layout: hoerbuch
title: Jacks Villa
cover: /images/hoerbuecher/jacks_villa.jpg
shop:
  apple: https://books.apple.com/de/audiobook/jacks-villa/id1493350589
  everand: https://de.everand.com/audiobook/441441183/Jacks-Villa
  google: https://play.google.com/store/audiobooks/details/Roland_Jesse_Jacks_Villa?id=AQAAAEBs4i-iUM
  storytel: https://www.storytel.com/de/de/books/1318775-Jacks-Villa
---
Autor: Roland Jesse  
Sprecher: Roland Jesse  
Spieldauer: 16 Minuten

# Inhalt

Jacks Villa ist wie ein Gefängnis. Zumindest für jene, die darin gefangen sind. So auch für das Mädchen in dieser Geschichte. Bis zu dem Tag, an dem sie eine Chance zur Flucht bekommt. In einem allgemeinen Trubel schleicht sie sich aus dem Haus und flieht in den nahe gelegenen Wald. Dort versteckt sie sich vorerst, um nie wieder in dieses Haus zurück zu kehren. Einmal entkommen, wird sie nie wieder einen Fuß dort hinein setzen.

Oder?
