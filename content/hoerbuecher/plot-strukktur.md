---
layout: hoerbuch
title: Plot und Struktur
subtitle: Kapitel 24 – Das perfekte auslösende Ereignis für Ihren Roman
cover: /images/hoerbuecher/plot-struktur.jpg
shop:
  amazon: https://www.amazon.de/Plot-Struktur-Meisterkurs-Romane-schreiben/dp/B08VDZHC7M/
  audible: https://www.audible.de/pd/Plot-Struktur-Hoerbuch/B08VDK17VX
  apple: https://books.apple.com/de/audiobook/plot-struktur-meisterkurs-romane-schreiben/id1547986295
  bookbeat: https://www.bookbeat.de/buch/plot-struktur-302545
  spotify: https://open.spotify.com/album/2X0LZuxMTMrwmPyiku9JVu?si=uQC28Sg1QCS2x7CLZVHCxA
audiosample:
  coverUrl: /images/hoerbuecher/plot-struktur.jpg
  mp3: /hoerproben/plot-struktur-24-das-perfekte-ausloesende-ereignis-fuer-ihren-roman.mp3
---
Autor: Stephan Waldscheidt  
Sprecher: Roland Jesse  
Spieldauer: 8 Stunden und 21 Minuten

# Inhalt

**So plotten sie dank perfekter Dramaturgie Ihren Bestseller.  
So optimieren Sie Struktur &amp; Dramaturgie Ihres Romans.**

In mehr als sechzig Kapiteln enthüllt dieser Ratgeber Schreibtipps und Tricks, wie Sie Plot und Struktur Ihres Romans optimieren – bei der Planung, beim Schreiben, bei der Überarbeitung. Jedes Kapitel macht Ihren Roman sofort besser. Erst wenn die Struktur funktioniert, kann sie das Panorama der von Ihnen erzählten Geschichte optimal tragen.Der Autorenratgeber eignet sich für Autoren, die ihren Roman vorab planen, hilft aber auch Autoren weiter, die den Roman erst beim Schreiben entdecken.

Das Buch orientiert sich an der klassischen und am weitesten verbreiteten Drei-Akte-Struktur, bietet darüber hinaus eine Menge Neues, was Sie nur in diesem Hörbuch finden.

**Mit 66 Musenküssen zur Inspiration und sofort in Ihrem Roman umsetzbar**

*»Ihre Schreibratgeber sind die besten mir bekannten auf dem Markt.«* – Kindle-Nr.1-Bestseller Isabell Schmitt-Egner

## Die wichtigsten Vorteile von »Plot &amp; Struktur«

* praktisch und sofort für Ihren Roman anwendbar
* viele Beispiele aus Romanen und Filmen
* Tipps, die Sie in keinem anderen Ratgeber finden
* Lösungen statt Regeln
* Inspiration und Anregungen für Ihre Ideen
* verständlich und unterhaltsam

**Aus der Reihe »Meisterkurs Romane schreiben — Schreibratgeber für Fortgeschrittene«**

*»Endlich mal ein Ratgeber für Fotgeschrittene, der wirklich anständig in die Tiefe geht. Top!!! Vor allem die detaillierte Beschreibung des 3-Akt-Modells habe ich so noch nirgendwo gelesen (...) Der Ratgeber gibt aber auch für Leute mit weniger Erfahrung eine ganze Menge gute Impulse.«* — Cara auf Amazon

## Gliederung

Das Buch bietet einen **Musenkuss** (mit Ideen und Fragen zu Ihrer Inspiration) zu jedem Kapitel.

Es gibt insgesamt 69 Kapitel, die sich in neun Teile gliedern:

### Intro

1. Titel und Sprecher  
2. Intro

### Erzählen &amp; Plotten

3. Die Grundlagen des Erzählens  
4. Die Struktur der drei Akte  
5. Ein einfacher Weg, Ihren Roman zu plotten  
6. So retten Sie Ihren plotgetriebenen Roman  
7. Richtig mit Erzählregeln brechen  
8. Konkrete Ziele im Roman  
9. Klarmachen: Ziele und Bedrohungen  
10. Wie wichtige Entscheidungen Ihren Roman verbessern  
11. Veränderungen zeigen, indem Sie sie nicht zeigen  
12. Die Wandlung des Helden zeigen  
13. So planen und plotten Sie richtig  
14. Geheimtipps zum Plotten  
15. Wie Sie von hinten nach vorn arbeiten  
16. Das Herz von Held und Bösewicht  
17. Opfer und Wahlen beim Plotten

### Erster Akt: Anfang &amp; Exposition

18. Der Ruf des Abenteuers und warum er funktioniert  
19. Wie die Heldin dem Ruf widersteht  
20. Risiko: Schockszenen zu Anfang Ihres Romans  
21. Von lauten und leisen Anfängen  
22. So schreiben Sie einen ersten Satz, der Leser umhaut  
23. So ziehen Sie jeden Leser in Ihren Roman - garantiert  
24. Das perfekte auslösende Ereignis für Ihren Roman  
25. Leser fangen Sie genau wie Wale  
26. Die Bedeutung von Zielen schon im ersten Akt  
27. Falsche und richtige Gründe für einen Prolog  
28. Die (Selbst-)Erkenntnis vor dem ersten Plotpoint  
29. Wendepunkte kenntlich machen  
30. Was den Wendepunkten vorausgehen sollte  
31. Wendepunkte eindringlicher machen

### Zweiter Akt: Eskalation

32. Der Gott des Gemetzels - nichts als Eskalation  
33. Keine Eskalation? (K)Ein Problem  
34. Warum Sie mal an Ihren Bösewicht erinnern sollten  
35. Der Midpoint - Das Kraftzentrum Ihres Romans  
36. So stärken Sie den Midpoint  
37. Sex at sixty - Emotional wichtig in der Romanstruktur  
38. Krise und Rekapitulation am Ende von Akt zwei  
39. Erläuterungen und Beispiele zum zweiten Plotpoint

### Dritter Akt: Resolution &amp; Ende

40. So machen Sie das Finale aufregender  
41. Stummschaltung für den Schurkenmonolog  
42. So holen Sie alles aus dem Höhepunkt heraus  
43. So schreiben Sie ein außergewöhnliches Ende  
44. Weitere Ideen für offene Enden      

### Erzählstränge &amp; Subplots

45. Erzählstränge aufwerten und retten  
46. So führen Sie Erzählstränge zusammen  
47. Die Kraft von Rahmenhandlungen  
48. Mit zweiter Ebene Romane verbessern und vertiefen  
49. Der Trimaran-Plot  
50. Erzählbögen wie Regenbögen      

### Szenen

51. Der perfekte Einstieg in eine Szene  
52. Der Twist am Ende einer Szene  
53. Die Bedeutsamkeit von Szenenzielen  
54. Aktion und Reaktion in den Enden von Szenen  
55. Nahtlose Übergänge schreiben  
56. Die Sterbeszene  
57. Wendepunkte in Szenen      

### Dichtere Geschichten weben

58. Dichtere, bessere Romane schreiben dank Assoziationen  
59. Retourkutschen und Bezüge  
60. Emotionen mittels Rückbezügen  
61. Wann Backstory zu Beginn sinnvoll ist  
62. Backstory versus Handlung im Jetzt  
63. Leitmotive  
64. Vorausblenden: echte und spekulative  
65. Hinweise verstecken und den Leser in die Irre führen  
66. Enthüllungen optimal präsentieren  
67. Aussaat: Hinweise / Ernte: Emotionen  
68. Auszahlungen: Ein Dankeschön an Ihre Leser        

      ### Abspann

69. Abspann
