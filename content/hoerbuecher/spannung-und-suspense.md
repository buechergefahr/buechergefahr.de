---
layout: hoerbuch
title: Spannung und Suspense
cover: /images/hoerbuecher/spannung_suspense.jpg
shop:
  amazon: https://www.amazon.de/Spannung-Suspense-Meisterkurs-Romane-schreiben/dp/B083RH3L4Z/
  audible: https://www.audible.de/pd/Spannung-Suspense-Hoerbuch/B084G2KNFX
  spotify: https://open.spotify.com/album/4QOahxwjToBcbeCsuzlI16?si=haKTFPp3RhWtpl3fsP086A
---
Autor: Stephan Waldscheidt  
Sprecher: Roland Jesse  
Spieldauer: 11 Stunden und 27 Minunten

# Inhalt

<strong>Spannung verstehen, Suspense begreifen und in jedem, auch Ihrem, Genre effektiv einsetzen – mit der Spannungsformel</strong>

So fesseln Sie Ihre Leser von der ersten bis zur letzten Seite Ihres Romans.

Spannung ist der Grund für den Leser, die Seiten Ihres Romans umzublättern. Darum gehört sie auf jede Seite und in jedes Genre, auch in Liebesromane oder anspruchsvolle Literatur. Oder wollen Sie etwa nicht, dass man Ihr Buch bis zum Ende liest?
    

Spannung ist nichts Ungreifbares, das beim Schreiben nebenher und zufällig entsteht. Sondern ein komplexes Instrument mit vielen Einstellknöpfen. Als AutorIn können Sie Spannung gezielt steuern – und das sollten Sie tun.

<strong>Wie Sie das anstellen und wie Ihr Roman mit jedem Knopfdruck besser wird, hören Sie in diesem Buch.</strong>

Wenn Konflikt der Treibstoff für einen Roman ist, so ist Spannung der Treibstoff für den Leser. Nervennahrung. Ohne gespannte Leser kein Roman. Und zwar in jedem Genre, Untergenre, Genremix, auch in literarischen Romanen.
    

<strong>Wie Sie den Leser ohne Pause in Atem halten, hören Sie in diesem Buch.</strong><br />
Was Sie in diesem Buch finden, finden Sie nirgendwo sonst. Versprochen.
    

Damit Spannung entstehen kann, braucht es eine Reihe von Faktoren. Die daraus resultierende Spannungsformel gibt Ihnen konkrete Instrumente an die Hand, mit denen Sie die Spannung in Ihrem Roman gezielt und auf vielfältige Weise beeinflussen können.
    

Die Spannungsformel bietet Ihnen neue, einzigartige Möglichkeiten, spannendere Bücher zu schreiben:

* Sie rücken die Spannung von einer mehr oder weniger zufälligen Nebenwirkung von Plot und Handlung dorthin, wo Sie hingehört: ins Zentrum Ihrer Aufmerksamkeit, Kreativität und Schreibarbeit. Und als Starkstrom in die Nerven Ihrer Leser.
* Durch die verschiedenen Faktoren können Sie die Spannung abwechslungsreicher gestalten, halten und steigern.
* Jeder der Faktoren beflügelt und befreit Ihre Kreativität. Mit der Spannungsformel wissen Sie besser, was Sie spannungstechnisch zu tun haben und wo Sie Ihre Ideen am besten sprießen lassen sollten.
* Die Auffächerung der Spannung in Faktoren sorgt dafür, dass die Arbeit für Sie überschaubarer wird.
* Die Spannungsformel ist ein hervorragender Ansatzpunkt für die Suche nach den Ursachen schwächelnder Spannung. Und, nach der Entdeckung, für deren Behebung.
* Jeder Faktor enthält selbst wieder Komponenten, an denen Sie drehen und so für mehr Spannung sorgen können.
* Die Formel dient neben der Veranschaulichung vor allem einem: dass Sie bequemer, übersichtlicher, variantenreicher und insgesamt besser mit Spannung arbeiten und Spannung steuern können.
    
Wie Sie die einzelnen Faktoren ausgestalten, dazu finden Sie detaillierte Tipps, Tricks und zahlreiche aktuelle Beispiele aus Buch und Film im Laufe dieses Buches.
In jedem Kapitel werden Sie auf anschauliche Weise neue Methoden entdecken, die Spannung in Ihrem Roman zu erhöhen, konkrete, sofort umsetzbare und immer wieder einsatzfähige Ideen.

Das Buch ist in vier Teile gegliedert:

### Teil 1: Spannung und die Spannungsformel

*Kapitel:*

1. Leser unter Strom: Wie auch Nicht-Elektriker spannende Romane schreiben
1. Plötzlich greift eine Möwe an: Wie Schock in die Spannungsformel passt
1. Es geht mal wieder um alles: »Ihre Einsätze, bitte« oder »Was auf dem Spiel steht«
1. Was droht? Tod! Sagen Sie klar, was auf dem Spiel steht
1. Rabiat zerrte der Drache mich fort: Dynamik als Spannungsfaktor und wie sie sich zusammensetzt
1. Ich würde nicht auf uns wetten: Risiko als Spannungsfaktor
1. Wird Katniss die Spiele überleben? Spannung ist eine Frage im Kopf des Lesers
1. Unversöhnlich am Telefon: Spannung als Anhäufung ungelöster Konflikte
1. Warum Rogue One langweilt: Was, wenn die wichtigste Zutat von Spannung fehlt?
1. So krass endet Game of Thrones: Spannung ist vor allem eine Sache des Lesers

### Teil 2: Wie Sie (mehr) Spannung erzeugen

*Kapitel:*

1. Spannung direkt vom Erzeuger: Wie Sie mit den richtigen Komponenten die Spannungsfaktoren vergrößern
1. Nicht klug, doch er kennt die Liebe: Nichts ist spannender für lesende Menschen als … Menschen
1. Unter der Treppe grüßt das Monster: Mit Urängsten für Spannung sorgen
1. Angst und Gewalt, sie leben hoch! Wie Evolution und Psychologie für spannende Romane sorgen
1. Ein Geheimnis. Und dann noch eins. Das Zauberwort bei der Erzeugung von Spannung im Roman
1. Chauffeur, Hiob, Leichenfinder: Mit unterschiedlichen Konflikten Ihren Roman spannender machen
1. Panther vs. Stäbe, Wanderer vs. Tod: Konflikte ausreizen! Bis aufs Blut!
1. Kämpfen? Fliehen? Heiraten? Morden? Wie Sie mit Entscheidungen Spannung erzeugen
1. Der Kleine mit der Schleuder da? Ha! Kontrast als Mittel zur Spannungserzeugung
1. And the winner is: die Emotion: Spannung erzeugen mit widerstreitenden Emotionen
1. Ich will keinen Anzug kaufen: Spannung erzeugen mittels Status
1. Kein Sex mit dem deutschen Spion: Wie Sie mit Verboten Spannung erzeugen
1. Spannung? Liegt in der Luft! Wie Sie mit Atmosphäre Spannung erzeugen
1. Die weise Stimme aus dem Off: Wie der auktoriale Erzähler Spannung erzeugt
1. Die Kerze erlischt: Spannung erzeugen, indem Sie etwas nicht schreiben
1. Diese Enge, Mann, sie bringt mich um! Spannung und Thema

### Teil 3: Suspense &amp; die Spannungsformel

*Kapitel:*

1. Hitchcocked: Was Suspense ist und kann
1. Alles unter Kontrolle! = Panik! So benutzen Sie die Erfahrungen der Leser zur Erzeugung von Suspense
1. Wann schnappt sie das nächste Opfer? Mit unterschwelliger Suspense konfliktarme Szenen aufwerten
1. Die Schwerter des Damokles: Bedrohungen als Mittel der Suspense
1. Meister der Suspense: Ihr Zahnarzt: Was Sie von einem Zahnarztbesuch für Ihren Roman lernen können
1. Ein Rassist mit abgesägter Hand: Wie Sie Suspense mit maximaler Wirkung auflösen
1. Nein, ich bin nicht George Kaplan: Die »Warum glaubt mir denn keiner«-Methode
1. Vulkanausbruch beim Smoothie: Eskalation: Suspense in Stufen. Ein Fallbeispiel.
1. Die berühmteste Pistole der Welt: Suspense mittels Requisiten
1. Prinzip Hoffnung: Suspense zwischen Hoffen und Bangen
1. Suspense mit dem Dampfhammer: Suspense mal weniger subtil, aber wirkungsvoll

#### Teil 4: Spannung nach Maß für jedes Genre

*Kapitel:*

1. Spannende Liebe, langweiliger Thrill: Spannung und Suspense sind genrespezifisch
1. Die Leidenschaft der Kommissarin: Was Sie tun können, wenn Ihr Roman ins falsche Genre geraten ist
1. Die Liebe ist wie ein Turnier: Spannung im Liebesroman/Romance
1. Verführt – Verbotenes Verlangen: Spannung im Erotik-Roman
1. Rätsel um einen unmöglichen Mord: Spannung im Krimi
1. Eine Bombe im Bus: Spannung im Thriller
1. Die Hebamme auf dem Scheiterhaufen: Spannung im Historischen Roman
1. Wenn der Elb gegen Zwerge kämpft: Spannung im Fantasy-Roman
1. Was lauert da im Schwarzen Loch? Spannung im SF-Roman
1. Geh bloß nicht in den Keller, Kind! Spannung im Horror-Roman
1. Ungewöhnliche Töne im Wahnsinn: Spannung in der literarischen Belletristik und dem Mainstream-Roman
1. Aufregende Jagd und glückliche Welt: Spannung im Kinderbuch
1. Außenseiter im Chaos der Gefühle: Spannung im Jugendbuch / Young Adult / New Adult
1. Story auf Steroiden: »All Age« – Ein Roman für jedes Alter. Mythos oder Realität?
1. Avatar des Lesers auf der Titanic: Gibt es Spannungsarten, die universell funktionieren?
