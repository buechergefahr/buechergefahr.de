---
title: Impressum
layout: page
permalink: /impressum/
---
**Büchergefahr**<br />
Tullastr. 58<br />
76131 Karlsruhe<br />
T: +49 156 78 31 28 79<br />
E: kontakt(at)buechergefahr.de

Inhaltlich Verantwortlicher gemäß &sect; 10 Absatz 3 MDStV:<br />
Roland Jesse (Anschrift wie oben)

### Urheberrechte

Bitte beachten Sie, dass Tonaufnahmen, Bilder und Texte auf diesen Seiten dem Urheberrecht unterliegen und nicht ohne
Genehmigung weiterverwendet werden dürfen.

### Haftungsausschluss

Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt
der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.