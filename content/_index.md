---
Title: Home
markup: html
type: home
kind: home
---
<section id="features">
	<div class="container">
		<header>

			<h2>Worum ging es hier mal?</h2>

		</header>
		<div class="row aln-center">

			<div class="col-4 col-6-medium col-12-small">
				<section>
					<a href="https://lektomat.de/" class="image featured icon solid"><img src="/images/lektomat_logo.svg"
							alt="Lektomat der Büchergefahr" /></a>
					<header>
						<h3><a href="https://lektomat.de/">Lektomat</a></h3>
					</header>
					<p>
						Ein Tool zum elektronischen Analysieren von Texten.
						Mit Satzlängen, Füllwörtern, Sentimenten.
					</p>
					<p>Ruht.</p>
				</section>

			</div>
			<div class="col-4 col-6-medium col-12-small">

				<section>
					<a href="/podcast/" class="image featured icon solid"><img src="/images/buechergefahr-logo.svg"
							alt="Büchergefahr-Podcast" /></a>
					<header>
						<h3><a href="/podcast/">Podcast</a></h3>
					</header>
					<p>
						Reden wir über den Lektomat, die Technik dahinter sowie Fragen des Sinns und Weltgeschehens.
					</p>
					<p>Schweigt.</p>
				</section>

			</div>
			<div class="col-4 col-6-medium col-12-small">

				<section>
					<a href="/hoerbuecher/" class="image featured icon solid"><img src="/images/hoerbuecher_logo.svg"
							alt="Büchergefahr-Hörbücher" /></a>
					<header>
						<h3>Hörbücher</h3>
					</header>
					<p>
						Warum nur über das Schreiben reden? Lesen geht doch auch, Vorlesen noch viel lieber.
					</p>
					<p>Keine neuen Produktionen geplant.</p>
				</section>
			</div>
		</div>

	</div>

</section>

<section id="banner">

	<div class="container">

		&nbsp;

	</div>
</section>