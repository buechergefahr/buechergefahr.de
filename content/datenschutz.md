---
title: Datenschutz
layout: page
permalink: /datenschutz/
---
### Datenschutzerklärung

#### Geltungsbereich

Diese Datenschutzerklärung informiert, welche personenbezogenen Daten der Seitenbesucher vom Betreiber dieser Seiten erfasst werden und wie er damit umgeht. Diese Datenschutzerklärung gilt für die Homepages <a href="https://buechergefahr.de/">buechergefahr.de</a>, <a href="https://lektomat.de/">Lektomat.de</a>, ProofReadZen.com, <a href="https://rolandjesse.de/">rolandjesse.de</a>, ragolds-loft(s).de und ragoldsloft(s).de inklusive aller Unterseiten und Subdomains.

#### Welche personenbezogenen Daten wir sammeln und warum wir sie sammeln

Unter <em>personenbezogenen Daten</em> werden Informationen verstanden, die einer bestimmten Person zugeordnet oder zur Identifizierung dieser Person genutzt werden können, unabhängig davon, ob diese Zuordnung bzw. Identifizierung direkt auf Basis dieser Daten oder auf Basis dieser Daten und anderer zusätzlicher Informationen erfolgt, auf die der Betreiber dieser Seiten Zugriff hat.

##### Zugriffsdaten / Server-Logdateien

Der Betreiber erhebt Daten über jeden Zugriff auf das Angebot in den Logdateien des Webservers, um den technischen Betrieb überwachen und im Fall von Störungen diese überprüfen und geeignet darauf reagieren zu können. Diese Zugriffsdaten beinhalten:

    * IP-Adresse des verwendeten Geräts, von welchem der Besuch erfolgt. Aus dieser lässt sich auch der (ungefähre) Standort des Besuchers ableiten.
    * Angaben zum Browsertyp
    * Angaben zum Internet Service Provider
    * Angaben zum verwendeten Betriebssystem
    * Zeitpunkt und Dauer des Besuchs

##### Dauer der Speicherung von Zugriffsdaten

Die Zugriffsdaten werden jeweils für eine Woche gespeichert und anschließend automatisch gelöscht.

#### Einbindung von Diensten und Inhalten Dritter

Auf den Webseiten dieser Homepage wird Inhalte Dritter, wie zum Beispiel Videos von Vimeo oder YouTube, Tweets von Twitter, Kartenmaterial von Google-Maps, RSS-Feeds oder Grafiken von anderen Webseiten eingebunden werden. Dies setzt immer voraus, dass die Anbieter dieser Inhalte (nachfolgend bezeichnet als »Dritt-Anbieter«) die IP-Adresse der Nutzer wahrnehmen. Denn ohne die IP-Adresse könnten sie die Inhalte nicht an den Browser des jeweiligen Nutzers senden. Die IP-Adresse ist damit für die Darstellung dieser Inhalte erforderlich.

Eingebettete Inhalte von anderen Websites verhalten sich exakt so, als ob der Besucher die andere Website besucht hätte. Diese Websites können Daten über den Besucher sammeln, Cookies benutzen, zusätzliche Tracking-Dienste von Dritten einbetten und die Interaktion von Besuchern mit diesem eingebetteten Inhalt aufzeichnen, inklusive der Interaktion mit dem eingebetteten Inhalt, falls Besucher ein Konto haben und auf dieser Website angemeldet sind.

Der Betreiber dieser Homepage ist bemüht, nur solche Inhalte zu verwenden, deren jeweilige Anbieter lediglich die IP-Adresse zur Auslieferung der Inhalte verwenden. Der Betreiber kann jedoch nicht ausschließen, dass die Dritt-Anbieter die IP-Adresse sowie gegebenenfalls jeweils eigene Cookies z.B. für statistische Zwecke verwenden und speichern.

#### Verwendung der Dienste der Fa. Steady Media UG

Unser Internetauftritt verwendet Dienste der Fa. Steady Media UG (haftungsbeschränkt), Schönhauser Allee 36, 10435 Berlin zur Abwicklung des Abonnementsgeschäfts der Prenzlauer Berg Nachrichten. Als Drittanbieter sammelt und speichert Steady zu diesem Zweck u.a. mögliche Identifizierungsdaten (u.a. IP-Adresse, Datum, Zeit und weitere technische Daten über den benutzten Internet-Browser und das benutzte Betriebsystem) und überprüft, ob ein Nutzer Abonnement der Prenzlauer Berg Nachrichten ist. Hierfür setzt die Fa. Steady Cookies auf unserer Website ein. Die vollständigen Datenschutzbestimmungen der Fa. Steady, die hier abgerufen werden können, gelten für diese Website entsprechend. 

Weitere Details gibt es direkt bei Steady H.Q. &rarr; <a href="https://steadyhq.com/de/privacy">https://steadyhq.com/de/privacy</a>

#### Einwilligung

Durch die Angabe zwingend benötigter und allenfalls weiterer freiwilliger Daten willigt die Besucherin / der Besucher im Rahmen der in dieser Datenschutzerklärung beschriebenen Zwecke in deren Erfassung, Verarbeitung, Verwendung und Weitergabe ein.

#### Löschen personenbezogener Daten

Besucher können die Löschung aller personenbezogenen Daten, die der Betreiber von ihnen gespeichert hat, anfordern. Dies umfasst nicht die Daten, die der Betreiber aufgrund administrativer, rechtlicher oder sicherheitsrelevanter Notwendigkeiten aufbewahren muss.

#### Weitergabe an Dritte

Die Weitergabe von Daten an Dritte erfolgt soweit dies für das Erbringen der Leistungen dieser Seiten sowie des Newsletters notwendig ist. Der Betreiber handelt und verkauft keine personenbezogenen Daten an Dritte.
