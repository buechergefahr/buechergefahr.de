---
title: 'Folge 76: Bücher auf die Ohren'
subtitle: Auf der Frankfurter Buchmesse mit Klaudia Zotzmann-Koch von ViennaWriter.net
summary: Buchmessen sind eine feine Gelegenheit, um einen etwas erweiterten Blick auf das aktuelle Geschehen der Welt rund um das Erzählen von Geschichten zu werfen. Mit der Autorin und Podcasterin Klaudia Zotzmann-Koch reden wir hier direkt auf der FBM19 über einen KI-Verlag, Hörbuchproduktionen und Datenschutzaspekte bei der Arbeit mit Manuskripten.
tags:
- kirschbuch verlag
- ki
- hoerbuch
- audio
- audiobook
- FBM19
- paula anders
- audible
- bookbeat
- NaNoWriMo
- NaNoWriMo2019
date: 2019-11-01T06:06:06
layout: post
author: Klaudia Zotzmann-Koch und Señor Rolando
explicit: clean
duration: '1:11:58'
episode_cover: "/images/buechergefahr_logo_300x300.jpg"
audio:
  mp3: buechergefahr76_fbm19_viennawriter.mp3
chapters:
- 00:00:00.000 Begrüßungsgeschrammel
- 00:00:05.798 Begrüßung
- 00:02:47.056 Vorstellung des Kirschbuch-Verlags auf der FBM19
- 00:11:05.760 Was läuft nicht so rund beim Anwenden von KI auf Texte?
- 00:16:53.585 Die DSGVO als Einladung zum Gespräch
- 00:21:14.261 Kommentare und Referenzen zu QualiFiction
- 00:23:06.051 Hörbuchproduktionen am Beispiel der Paula-Anders-Reihe von Klaudia
- 00:28:20.966 Toolunterstützungen für Autor*innen
- 00:33:24.412 Computergenerierte Sprache
- 00:35:26.847 Virtuelle Influencer*innen und Interaktion mit ihnen
- 00:39:50.652 Audio first
- 00:47:19.522 Das Shelf-Life von Hörbüchern
- 00:53:38.343 Wie easy ist es eigentlich, Texte einzusprechen?
- 01:07:26.911 Über Strategien und Pläne zur Paula-Anders-Reihe von Klaudia
- 01:10:20.731 Outro und Verabschiedung
---
Buchmessen sind eine feine Gelegenheit, um einen etwas erweiterten Blick auf das aktuelle Geschehen der Welt rund um das Erzählen von Geschichten zu werfen. Mit der Autorin und Podcasterin [Klaudia Zotzmann-Koch](https://www.zotzmann-koch.com/) reden wir hier direkt auf der [#FBM19](https://www.buchmesse.de/) über einen KI-Verlag, Hörbuchproduktionen und Datenschutzaspekte bei der Arbeit mit Manuskripten.

# Links

* [Klaudias Vienna Writer's Blog 'n' Podcast](https://www.viennawriter.net/)
* [Frankfurter Buchmesse](https://www.buchmesse.de/)
* [Kirschbuch Verlag](https://www.kirschbuch-verlag.de/)
* [LiSA – die Textanalysesoftware von QualiFiction](https://www.qualifiction.info/lisa/)
* [John Truby](http://truby.com/)
* [Inhaltsverzeichnis der Ausgabe 4/2018 des Magazins »der selfpublisher« mitsamt des Beitrags »Künstliche Intelligenz für Selfpublisher«](https://www.autorenwelt.de/magazin/der-selfpublisher/archiv/der-selfpublisher-42018)
* [Folge 67 der Büchergefahr mit Fragen an eine Künstliche Intelligenz](https://buechergefahr.de/67/#44a67687)
* [Folge 73 der Büchergefahr mit Vorstellung des Lektomats](https://buechergefahr.de/73/#f944cbf2)
* [Folge 75 der Büchergefahr mit einer Unterscheidung zwischen passiver und aktiver KI](https://buechergefahr.de/75/#34e4c87c)
* [Die Paula Anders Reihe von Klaudia](https://www.zotzmann-koch.com/buecher/die-paula-anders-reihe/)
* [Findaway Voices](https://findawayvoices.com/)
* [Podcast von Joanna Penn](https://www.thecreativepenn.com/podcasts/)
* [Papyrus Autor](https://www.papyrus.de/)
* [OpenAI und ihr Handling von GPT-2](https://openai.com/blog/fine-tuning-gpt-2/)
* [About OpenAI](https://openai.com/about/)
* [Blick über den Tellerrand mit Alex Wunschel](https://www.pimpyourbrain.de/)
* [Blick 322 auf virtuelle Influencer*innen](https://tellerrand.podigee.io/322-virtuelle-influencer)
* [Zündfunk Generator bei Bayern 2](https://www.br.de/mediathek/podcast/zuendfunk-generator/478)
* ["Alexa, liebst du mich?" Gefühle zwischen Mensch und Maschine – Zündfunk Generator](https://www.br.de/mediathek/podcast/zuendfunk-generator/alexa-liebst-du-mich-gefuehle-zwischen-mensch-und-maschine/1728582)
* [Muss ich nett zu einer KI sein? – Zündfunk Generator](https://www.br.de/radio/bayern2/sendungen/zuendfunk/generator-ki-gefuehle-zwischen-mensch-und-maschine-100.html)
* [Wie man Freunde gewinnt – Dale Carnegie](https://unternehmerkanal.de/buecher-2/persoenlichkeitsentwicklung/wie-man-freunde-gewinnt-dale-carnegie/)
* [Audible](https://www.audible.de/)
* [BookBeat](https://www.bookbeat.de/hoerbuecher)
* [NaNoWriMo](https://nanowrimo.org/)
