---
layout: post
title: "Podcasts für Selfpublisher – die Folien von der Frankfurter Buchmesse"
date: 2016-10-22T22:59:25+02:00
comment_note: true
---
Wie <a href="http://buechergefahr.de/30/">in Folge 30 angekündigt</a> war dieser Kanal <a href="http://catalog.services.book-fair.com/de/veranstaltungen/tagesuebersicht/veranstaltung/action_eventcalendar/detail/controller_eventcalendar/Eventcalendar/objid_eventcalendar/1301/">auf der Frankfurter Buchmesse</a>. Passenderweise mit einem Vortrag zu <a href="http://www.slideshare.net/gchq/podcasts-fr-selfpublisher">Podcasts für Selfpublisher</a>. Wie während des Vortrags versprochen gibt es hier die gezeigten Folien:

<iframe src="//www.slideshare.net/slideshow/embed_code/key/yxPgWefDpCy4Hf" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allow="fullscreen"> </iframe>

Eine Folge mit dem gesprochenen Wort dazu kommt noch. Vorher sind jedoch ein paar Interviews an der Reihe, welche ebenfalls auf der Buchmesse entstanden sind.
