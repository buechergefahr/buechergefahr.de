---
layout: post
title: "Folge 31: Buch gegen Daten"
date: 2016-10-21T23:23:23+02:00
duration: "13:40"
tags:
- Buchmesse Frankfurt
- FBM16
- Readfy
- Jellybooks
- Tracking
- Nutzerverhalten
- Datenschutz
summary: Readfy stellt auf der Frankfurter Buchmesse die Reader Analytics vor und liefert damit Details zum Leseverhalten.
author: Señor Rolando
explicit: 'no'
comment_note: true
episode_cover: "/images/bg_episode31_300x300.jpg"
audio:
  mp3: bg_folge31_buch_gegen_daten.mp3
chapters:
  - 00:00:00.000 Episode 2.
---
## Links

* [Frankfurter Buchmesse](http://www.buchmesse.de/de/fbm/)
* [Folge 19: Jellybooks](http://buechergefahr.de/19/)
* [Readfy mit den Reader Analytics](https://www.readfy.com/media/filer_public/59/c4/59c4fe6d-dfa1-49d5-9b34-629a485ce630/pm_readfy_reader_analytics_final.pdf) (PDF)
* [Datenschutzerklärung von Readfy](https://www.readfy.com/de/datenschutz/) mit §2.2 zu »Tracking-Informationen, die während der Nutzung unserer Angebote gesammelt werden«
* [Bookcrossing](http://www.bookcrossing.com/howto)
