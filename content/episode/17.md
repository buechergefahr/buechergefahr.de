---
layout: post
title: "Folge 17: 7 Voraussetzungen für den Erfolg beim Schreiben"
date: 2016-01-31
duration: "14:22"
tags:
- Gregory Bassham
- Tom Morris
- Motivation
- Ziele
- Vorsätze
description: Schreibtipps gibt es überall, selbst in philosophischen Büchern über das Laufen.
author: Señor Rolando
explicit: 'no'
comment_note: true
episode_cover: "/images/bg_episode17_300x300.jpg"
audio:
  mp3: bg_folge17_7voraussetzungen.mp3
chapters:
  - '00:00:00.000 Episode 17.'
---
## Das Buch

* [Die Philosophie des Laufens](http://www.mairisch.de/programm/michael-w-austin-p-reichenbach-die-philosophie-des-laufens/)

## Die 7 Voraussetzungen

1. Voraussetzung: Eine klare, lebhafte Vision unseres Ziels
2. Voraussetzung: Wir müssen selbstbewusst sein, wenn wir unsere Ziele erreichen wollen
3. Voraussetzung: Wir müssen uns ganz und gar auf das konzentrieren, was notwendig ist, um das Ziel zu erreichen
4. Voraussetzung: Wir müssen unsere Vision mit sturer Beharrlichkeit verfolgen
5. Voraussetzung: Wir brauchen eine emotionale Verbindung zu dem, was wir tun
6. Voraussetzung: Wir brauchen einen guten Charakter, der uns führt und hilft, auf dem richtigen Weg zu bleiben
7. Voraussetzung: Wir müssen offen dafür sein, unsere eigene Entwicklung zu genießen

## Zitierte Personen und Ideen

* [Gregory Bassham](http://staff.kings.edu/ghbassha/index.htm)
* (nicht mehr verfügbar:) *sieben Voraussetzungen für Erfolg* von *Tom Morris*
* [7 Wege zur Effektivität](https://de.wikipedia.org/wiki/The_Seven_Habits_of_Highly_Effective_People) von [Stephen Covey](http://www.stephencovey.com/)
* [Ralph Waldo Emerson](https://de.wikipedia.org/wiki/Ralph_Waldo_Emerson)
* [Francis Bacon](https://de.wikipedia.org/wiki/Francis_Bacon)
* [George Sheehan](http://www.georgesheehan.com/)
* [Platon](https://de.wikipedia.org/wiki/Platon)
* [Flow](https://de.wikipedia.org/wiki/Flow_(Psychologie))
* [Hedonistisches Paradox](http://zenstoiker.blogspot.de/2014/04/das-hedonistische-paradox.html)
