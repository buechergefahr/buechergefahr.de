---
layout: post
title: "Folge 12: Von Texten und dem Verlegen"
date: 2015-06-27
duration: "8:26"
tags:
- Verlage
- Selbstpublizieren
- Steintafeln
- Risiko
- Crowdfunding
description: Das Verlegen von Texten ändert sich im Laufe der Zeit. Das war so, ist so und bleibt vielleicht auch so.
author: Señor Rolando
explicit: 'no'
comment_note: true
episode_cover: "/images/bg_episode12_300x300.jpg"
audio:
  mp3: bg_folge12_verlegen.mp3
chapters:
  - '00:00:00.000 Episode 12.'
---
## Erwähnte Links

* [Gutenbergs Lettern und die Druckerpresse](http://www.gutenberg.de/erfindun.htm)
* [Verlage seit ca. 120 Jahren relevant](https://de.wikipedia.org/wiki/Verlag#Geschichte)
* [Dieses Crowdfunding von Büchern](http://sr-rolando.com/editorial/2015/06/25/dieses-crowdfuning-von-buechern/)
* [Die Zukunft des Lesens](https://www.perlentaucher.de/essay/ueber-die-zukunft-des-lesens.html) bei den Perlentauchern
* [Wir können nicht mehr Lesen](https://medium.com/deutsch/warum-k%C3%B6nnen-wir-nicht-mehr-lesen-ec2117d79a79)
