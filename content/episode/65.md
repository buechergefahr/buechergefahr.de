---
title: 'Folge 65: Erste KI-Begriffsdefinitionen'
subtitle: Begriffe, Ziele, Methoden und etwas Ausblick
summary: In dieser Folge gibt es einen ersten Einstieg in das Thema der KI für Autorinnen.
tags:
- KI
- starke KI
- schwache KI
- Lernen
- Maschinelles Lernen
- Trainieren
- Frankfurter Buchmesse
date: 2018-10-01T20:20:20
layout: post
author: Señor Rolando
explicit: 'clean'
duration: '08:08'
episode_cover: "/images/buechergefahr_logo_300x300.jpg"
audio:
  mp3: buechergefahr65_ki_datenmengen.mp3
chapters:
- 00:00:05.897 Begrüßung
- 00:00:27.245 Begriffe? Begriffe.
- 00:06:10.455 Ausblick
- 00:06:42.833 Ankündigung zur FBM18
- 00:07:34.206 Verabschiedung
---
Steigen wir ganz vorsichtig und dezent in das Thema der Künstlichen Intelligenz für AutorInnen ein. Und machen wir das natürlich mit einer kleinen Begriffsdefinition. Aber es ist eine zaghafte, eine kleine nur. Die komplizierte Technik heben wir uns für spätere Folgen auf. Und doch geht es schon um Big Data, geht es um maschinelles Lernen, geht es um das Trainieren des maschinellen Lernens. Immerhin geht es noch nicht wirklich um neuronale Netze, das wäre doch unangemessen für einen sanften und ruhigen Einstieg.

## Link

* [Pragmatische KI für AutorInnen: Session auf der Frankfurter Buchmesse](https://blog.tolino-media.de/2018/09/unsere-events-auf-der-frankfurter-buchmesse-2018/) (am Freitag, den 12. Oktober 2018, 11:00 - 12:00 Uhr bei tolino media in Halle 3.0, Stand H2)
