---
layout: post
title: "Folge 34: Podcasting für Selfpublisher"
date: 2016-11-25T07:30:00+01:00
duration: "30:00"
tags:
- Buchmesse Frankfurt
- FBM16
- Tolino Media
- Podcasting
- Selfpublishing
- Marketing
- Mikrofone
- Ultraschall
- Motivation
summary: Auf der Frankfurter Buchmesse hat Sr. Rolando etwas über das Podcasting für Selfpublisher erzählt.
author: Señor Rolando
explicit: 'no'
comment_note: true
episode_cover: "/images/bg_episode34_300x300.jpg"
audio:
  mp3: bg_folge34_fbm16_podcast_session.mp3
chapters:
  - 00:00:00.000 Episode 34.
---
## Die Slides

  <iframe src="//www.slideshare.net/slideshow/embed_code/key/yxPgWefDpCy4Hf" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allow="fullscreen"> </iframe> <div style="margin-bottom:5px" />

## Links

* [Tolino Media](https://www.tolino-media.de/)
* Mikrofon [Samsung Go Mic](http://www.samsontech.com/samson/products/microphones/usb-microphones/gomic/) (z.B. [bei Thomann](https://www.thomann.de/de/samson_go_mic.htm))
* Mikrofon [Røde Podcaster](http://de.rode.com/microphones/podcaster) (z.B. [bei Thomann](https://www.thomann.de/de/rode_podcaster.htm))
* Mikrofon [Shure SM58](http://www.shure.de/produkte/mikrofone/sm58) (z.B. [bei Thomann](https://www.thomann.de/de/shure_sm58.htm))
* Rekorder [Zoom H5](https://www.zoom-na.com/products/field-video-recording/field-recording/zoom-h5-handy-recorder) (z.B. [bei Thomann](https://www.thomann.de/de/zoom_h5.htm))
* [QuickTime](http://www.apple.com/de/quicktime/what-is/)
* [Audacity](http://www.audacity.de)
* [Ultraschall](http://ultraschall.fm) auf Basis von [Reaper](http://www.reaper.fm/)
* Apples [Logic Pro X](http://www.apple.com/de/logic-pro/)
* Adobe [Audition](http://www.adobe.com/de/products/audition.html)
* [Wordpress](https://wordpress.org/)
* [Podlove](http://podlove.org/)
* [Octopod](https://github.com/pattex/octopod)
* [Jurgen Appelo](http://jurgenappelo.com/about/): [Multitasking Is Bad, Multiprojecting Is Good](http://noop.nl/2015/09/multitasking-is-bad-multiprojecting-is-good.html)
* [Auphonic](https://auphonic.com/)
* [eCamm Call Recorder](http://www.ecamm.com/mac/callrecorder/) für Skype
* [FAQ zum Aufzeichnen von Skype-Anrufen](https://support.skype.com/de/faq/FA12395/wie-kann-ich-meine-skype-anrufe-aufzeichnen)
* [Studio-Link](http://studio-link.de/) (auch [ohne viel Aufwand beim Gegenüber](https://doku.studio-link.de/standalone/installation-standalone.html))
* [Zencastr](https://zencastr.com/)
