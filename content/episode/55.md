---
title: 'Folge 55: Alltag einer Autorin'
subtitle: 'Im Gespräch mit Nike Leonhard'
description: Wie sieht der Alltag einer Selfpublishing-Autorin aus, die umfangreiche Fantasy-Geschichten schreibt? Schafft sie auch, das Marketing komplett selbst zu machen und ist das überhaupt sinnvoll? Organisiert sie sich in Netzwerken? Und was sind eigentlich normale und übliche Textlängen? Wie entwickel(t)en sich diese über die Zeit? Viele Fragen, hier in der Folge gibt es mögliche Antworten.
tags:
- fantasy
- selfpublishing
- marketing
- saga
- romane
- kurzgeschichten
- buchformen
- netzwerke
- nornennetz
date: 2017-09-04T12:22:22+01:00
layout: post
author: Nike Leonhard und Señor Rolando
explicit: 'no'
duration: '0:27:27'
episode_cover: "/images/bg_episode55_300x300.jpg"
audio:
  mp3: buechergefahr55_nike_leonhard.mp3
chapters:
- 00:00:06.347 Intro
- 00:01:36.465 Begrüßung
- 00:02:38.936 Trends bei Textlängen
- 00:03:36.325 Serialisierung vs. bündelnde Buchthemen
- 00:06:43.957 Historie unterschiedlicher Textlängen
- 00:09:23.108 Alltag als Selfpublisherin
- 00:11:09.597 Sinnvolle Marketinginstrumente einer Selfpulisherin
- 00:12:55.898 Netzwerke für Autorinnen
- 00:16:18.582 Der Kollateralnutzen von Netzwerken
- 00:17:27.202 Über das Auslagern von Marketingaktivitäten
- 00:20:56.325 Mut als Autorin
- 00:24:02.084 Outro
---
Wir geben eine mögliche weitere Spielart für das Finden, das Entdecken von Texten. Wie können zum Beispiel Empfehlungen aussehen, die wertig und nicht einfach plumpe Anpreisung sind? Und was ist überhaupt »Kollateralnutzen«? Auch das klären wir auf.

So ganz en passant überlegen wir, wann sich das Auslagern von Marketingaktivitäten potenziell lohnt, ob es ratsam ist, als Autorin politisch zu werden und sich zum Weltgeschehen zu äußern sowie was eigentlich allgemein übliche Textlängen sind und wie sich diese über die Zeit entwickelt haben.

# Links

* [Blog und Homepage von Nike Leonhard mit Infos zu ihren Texten](https://nikeleonhard.wordpress.com/)
  * [Nike auf Twitter](https://twitter.com/nike_leonhard)
  * [Nike auf Facebook](https://www.facebook.com/Nike-Leonhard-608462059308830/)
  * [Codex Aureos](https://nikeleonhard.wordpress.com/codex-aureus-phantastisch-historisch-erzaehlungen-ebooks-nike-leonhard/) inklusive [Steppenbrand](https://nikeleonhard.wordpress.com/codex-aureus-phantastisch-historisch-erzaehlungen-ebooks-nike-leonhard/steppenbrand-codex-aureus-2/)
* [George R. R. Martin](http://www.georgerrmartin.com/)
* [Die Nebel von Avalon](https://de.wikipedia.org/wiki/Die_Nebel_von_Avalon)
* [Nornennetzwerk](http://nornennetz.aeom.de/)
* [Folge 6 mit Steffen Meier als Gast](http://buechergefahr.de/6/#a884a1a5)
* [Steffen Meier im Sonntagsgespräch beim BuchMarkt](https://www.buchmarkt.de/menschen/steffen-meier-ist-genervt-es-werden-gegensaetze-geschaffen-die-gar-keine-sind/)
* [@Buechergefahr auf Twitter](https://twitter.com/Buechergefahr)
* [Dieser Beitrag zum Kommentieren auf Facebook](https://www.facebook.com/senor.rolando/posts/10157378069501959)
