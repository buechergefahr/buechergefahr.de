---
layout: default
title: Kontakt
permalink: "/kontakt/"
---
<section id="main">
  <div class="container">
    <div class="row aln-center">
      <div class="col-6 col-12-small">
        <header>
        <h2 class="centered">Kontakt</h2>
        </header>
        <ul class="icons">
          <li class="icon solid fa-envelope"><strong>E-Mail:</strong> <a href="mailto:kontakt(at)buechergefahr.de">kontakt(at)buechergefahr.de</a></li>
          <li class="icon brands fa-twitter"><strong>Twitter:</strong> <a href="https://twitter.com/buechergefahr/">@buechergefahr</a></li>
          <li class="icon brands fa-mastodon"><strong>Mastodon:</strong> <a href="https://literatur.social/@buechergefahr">@buechergefahr</a></li>
          <li class="icon brands fa-instagram"><strong>Instagram:</strong> <a href="https://www.instagram.com/buechergefahr/">@buechergefahr</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
